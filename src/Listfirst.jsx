import { useState } from 'react';
// import './index.css';

const Listfirst = () => {

  const [list,setList] = useState("");
  const [Items,setItems] = useState([]);
  const inputEvent = (event) => {
    let value = event.target.value;
    setList(value);
  }

  const listofItems = () => {
    setItems((oldItems) => {
      return [...oldItems,list];
    })
    setList('');
  }

  const deleteItem = (id) => {
    setItems((oldItems) => {
      return oldItems.filter((arrEle,index) => {
        return index !== id;
      })
    })
  }
  return (
    <div className="main_div">
    <div className="center_div">
      <br/>
      <h1>ToDo List</h1>
      <br/>
      <input type="text" name="list" placeholder="Add a Items" onChange={inputEvent} value={list}/>
      <button onClick={listofItems}>+</button>

      <ol>
        {Items.map((item,index)=>{
          return (
            <>
            <div className="todo_style">
            <i className="fa fa-times" onClick={() => deleteItem(index)} ></i>
            <li key={index}>{item}</li>
            </div>
            </>
            )
        })}
      </ol>
    </div>
      
    </div>
  );
}

export default Listfirst;

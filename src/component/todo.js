import React, { useEffect, useState } from 'react';
import './todo.css';
import todo from '../img/todo.svg';

 // get data from localstorage

 const getItem = () => {
   let list = localStorage.getItem('lists');
   if(list){
    return JSON.parse(localStorage.getItem('lists'))
   }else{
     return [];
   }
 }

const  Todo = () => {
  const [inputData,setInputdata] = useState("");
  const [items,setItems] = useState(getItem());
  const [toggleBtn,setToggleBtn] = useState(true);
  const [isEditItem,setIsEditItem] = useState(null);

  const addList = () => {
    if(!inputData)
    {
      alert("Please enter data");
    }else if(inputData && !toggleBtn){
       setItems(
        items.map((item)=>{
          if(item.id === isEditItem){
            return{...item,name:inputData}
          }
           return item; 
        })
      )
      setToggleBtn(true);
      setInputdata('');
      setIsEditItem(null);

    }
    else{
      const allInputData = {id: new Date().getTime().toString(),name:inputData}
      setItems([...items,allInputData]);
      setInputdata("")
    }
  }

  const deleteItem = (id) => {
    setItems((oldVal) => {
      return oldVal.filter((item) => {
        return item.id !== id;
      })
    })
  }

    const editItem = (id) => {
      let newEditItem = items.find((item) => {
        return item.id === id;
      })
      setToggleBtn(false);
      setInputdata(newEditItem.name);
      setIsEditItem(id);
    }
 

  const deleteItems = () => {
    setItems([]);
  }

  // set date into localstorage
  useEffect(() => {
    localStorage.setItem('lists',JSON.stringify(items));
  })

  return (
    <>
    <div className='main-div'>
      <div className='child-div'>
        <figure>
          <img src={todo} alt="todologo" />
          <figcaption>Add Your List here 🤘</figcaption>
        </figure>

        <div className="addItems">
          <input type="text" placeholder='✍️ Add Items' value={inputData} onChange={(e)=>{
            setInputdata(e.target.value)
          }}/>
          { toggleBtn ? <i className='fa fa-plus add-btn' title='Add Item' onClick={addList}></i>:<i className='fa fa-edit' title='Edit Item' onClick={addList}></i>}
          
        </div>
        <div className="showItems">
          {items.map((item) => {
            return(
              <>
                <div className="eachItem" key={item.id}>
                <h3>{item.name}</h3>
                <div className='todo-btn'>
                <i className="fa fa-edit" title='Edit Item' onClick={()=> editItem(item.id)}></i>
                <i className='fa fa-trash' title='Delete Item' onClick={()=> deleteItem(item.id)}></i> 
                </div>
                
                </div>
              </>
            )
          })}
          </div>
        
      { items.length > 0 &&
        <div className="showItems">
          <button className="btn effect04" onClick={deleteItems} data-sm-link-text="Remove All"><span>Check List</span></button>
        </div>
      }
      </div>
    </div>
    </>
  )
}

export default Todo;
